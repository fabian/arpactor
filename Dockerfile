FROM alpine:latest

ENV APP_ROOT /app

COPY . $APP_ROOT/

WORKDIR $APP_ROOT

RUN apk add --no-cache \
    python

CMD ["python", "arpactor/main.py"]
