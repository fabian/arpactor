from actor import Actor


class DashActor(Actor):
    """
    A sample Amazon Dash Button actor which just prints "Dash Button pressed!"
    once a press of my Cottonelle Dash Button got recognized.
    """
    MAC = '50f5da5b5937'

    def act(self):
        print("Dash Button pressed!")
