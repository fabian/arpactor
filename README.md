# ARPACTOR
A framework to react on ARP packets from certain MAC addresses.

Example:
You own an Amazon Dash Button, a little Wifi enabled IoT-Button that
is typically used to order stuff from Amazon. You configured it to
be part of your Wifi network, but did not select a specific product
while setting it up, so it does not actually buy stuff. If you press 
it, it wakes up and sends ARP requests to your network to obtain an IP 
address. Arpactor allows you to register an actor on its MAC address
and lets you trigger custom defined actors. Like send you an email once
it gets pressed.

# Create your first Actor and start spoofing button presses
Let's say you own a Amazon Dash button, looked up its MAC address
on your router's web interface and want to print to the console 
when a button press got detected.

Step 1: Create a directory for your actors, e.g. /opt/my_actors/
```
mkdir -p /opt/my_actors
```

Step 2: Inside this directory, create any amount of python files with
actors that you wrote, e.g. /opt/my_actors/my_dash_console_printer.py.
You can define multiple actors for the same MAC, of course. The path
of the actors gets passed to arpactor, which then imports and activates
all of them automatically.
```
from actor import Actor


class DashActor(Actor):
    MAC = '50f5da5b5937'  # The MAC address of your Dash Button

    def act(self):
        print("Dash Button pressed!")
```

Step 3:
Run arpactor/main.py with a Python 2 interpreter as root. You have
two options to pass the path of the actors:

1. Define a environment variable ARPACTOR_ACTORS_PATH
2. Pass it to main.py using the -a or --actors_path flag

arpactor then starts spoofing button presses and executes your
actors. Have fun!


# Experiment with Docker
It's also possible to build and run arpactor inside a docker container.

## Build arpactor/demo container
```
sudo docker build -t arpactor/demo .
```

## Run container with your own actors
```
sudo docker run \
    -ti \
    --net=host \
    -e "ARPACTOR_ACTORS_PATH=/app/actors" \
    --volume=/opt/my_actors:/app/actors \
    arpactor/demo 
```
